package it.netgrid.woocommerce.jersey;

import it.netgrid.woocommerce.BulkService;
import it.netgrid.woocommerce.Configuration;
import it.netgrid.woocommerce.jersey.bulk.CouponBulkService;
import it.netgrid.woocommerce.jersey.bulk.CustomerBulkService;
import it.netgrid.woocommerce.jersey.bulk.OrderBulkService;
import it.netgrid.woocommerce.jersey.bulk.OrderNoteBulkService;
import it.netgrid.woocommerce.jersey.bulk.OrderRefundBulkService;
import it.netgrid.woocommerce.jersey.bulk.ProductAttributeBulkService;
import it.netgrid.woocommerce.jersey.bulk.ProductAttributeTermBulkService;
import it.netgrid.woocommerce.jersey.bulk.ProductBulkService;
import it.netgrid.woocommerce.jersey.bulk.ProductCategoryBulkService;
import it.netgrid.woocommerce.jersey.bulk.ProductOrderBulkService;
import it.netgrid.woocommerce.jersey.bulk.ProductReviewBulkService;
import it.netgrid.woocommerce.jersey.bulk.ProductShippingClassBulkService;
import it.netgrid.woocommerce.jersey.bulk.ProductTagBulkService;
import it.netgrid.woocommerce.model.Coupon;
import it.netgrid.woocommerce.model.Customer;
import it.netgrid.woocommerce.model.Order;
import it.netgrid.woocommerce.model.OrderNote;
import it.netgrid.woocommerce.model.OrderRefund;
import it.netgrid.woocommerce.model.Product;
import it.netgrid.woocommerce.model.ProductAttribute;
import it.netgrid.woocommerce.model.ProductAttributeTerm;
import it.netgrid.woocommerce.model.ProductCategory;
import it.netgrid.woocommerce.model.ProductReview;
import it.netgrid.woocommerce.model.ProductShippingClass;
import it.netgrid.woocommerce.model.ProductTag;

public class BulkServiceManager {

	public static BulkService<Coupon, Integer, Object> createCouponBulkService(Configuration config) {
		return new CouponBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<Customer, Integer, Object> createCustomerBulkService(Configuration config) {
		return new CustomerBulkService(CrudServiceManager.getWebTarget(config));
	}
		
	public static BulkService<Order, Integer, Object> createOrderBulkService(Configuration config) {
		return new OrderBulkService(CrudServiceManager.getWebTarget(config));		
	}
	
	public static BulkService<OrderNote, Integer, Order> createOrderNoteBulkService(Configuration config) {
		return new OrderNoteBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<OrderRefund, Integer, Order> createOrderRefundsBulkService(Configuration config) {
		return new OrderRefundBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<ProductAttribute, Integer, Object> createProductAttributeBulkService(Configuration config) {
		return new ProductAttributeBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<ProductAttributeTerm, Integer, ProductAttribute> createProductAttributeTermBulkService(Configuration config) {
		return new ProductAttributeTermBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<Product, Integer, Object> createProductBulkService(Configuration config) {
		return new ProductBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<ProductCategory, Integer, Object> createProductCategoryBulkService(Configuration config) {
		return new ProductCategoryBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<Order, Integer, Product> createProductOrderBulkService(Configuration config) {
		return new ProductOrderBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<ProductReview, Integer, Product> createProductReviewBulkService(Configuration config) {
		return new ProductReviewBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<ProductShippingClass, Integer, Object> createProductShippingClassBulkService(Configuration config) {
		return new ProductShippingClassBulkService(CrudServiceManager.getWebTarget(config));
	}
	
	public static BulkService<ProductTag, Integer, Object> createProductTagBulkService(Configuration config) {
		return new ProductTagBulkService(CrudServiceManager.getWebTarget(config));
	}
}
